package model;

public class Postagem {
	public Postagem(String titulo, String dataPostagem) {
		this.titulo = titulo;
		DataPostagem = dataPostagem;
	}
	private String titulo;
	private String DataPostagem;
	
	public String getTitulo() {
		return titulo;
	}
	public String getDataPostagem() {
		return DataPostagem;
	}
}
