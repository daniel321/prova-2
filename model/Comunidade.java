package model;

public class Comunidade {
	public Comunidade(String nome) {
		this.nome = nome;
	}
	private String nome;
	private String tipo;
	private ArrayList<Usuario> listaDeUsuarios = new ArrayList<Usuario>();
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
