package model;
import java.util.ArrayList;

public class RedeSocial extends Portal {
	
	private ArrayList<Participante> listaDeUsuarios = new ArrayList<Participante>();
	private ArrayList<Comunidade> listaDeComunidades = new ArrayList<Comunidade>();
	private ArrayList<Postagem> listaDePostagens = new ArrayList<Postagem>();
	
	public void adicionarComunidade(Comunidade comunidade) {
		listaDeComunidades.add(comunidade);
	}
	public void adicionarUsuario(Participante participante) {
		listaDeUsuarios.add(participante);
	}
	public void adicionarPostagem(Postagem postagem) {
		listaDePostagens.add(postagem);
	}
}
