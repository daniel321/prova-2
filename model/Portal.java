package model;

public class Portal {
	public Portal(String nome) {
		this.nome = nome;
		this.endereco = "http://" + this.nome + ".com"; 
	}
	private String nome;
	private String endereco;
	
	public String getNome() {
		return nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String prefixo,String endereco,String sufixo) {
		if(prefixo == null) prefixo = "www.";
		else prefixo = prefixo + ".";
		if(sufixo == null) prefixo = "com";
		this.endereco = "http://" + prefixo + endereco + "." + sufixo;
	}
	
}
